U�ycie:

python simplex_achpl.py

Rozwiazywania graficzne: Sa dostepne dla funkcji z max 2 zmiennymi, przyklad dzialania -> test2

1. Pierwszy wybor to: 
> - plik -> wczytanie .mat 
> - user -> wczytanie wlasnej funkcji
> - test1 -> dla testow, mozna skorzysta� aby nie wprowadza� danych recznie, 3 zmienne
> - test2 -> dla testow, mozna skorzysta� aby nie wprowadza� danych recznie, 2 zmienne + graph
	
Jesli user:
	
2. Podajemy max lub min, w zaleznosci co chcemy z dana funkcja robic.

3. Podajemy liczbe zmiennych w funkcji.

4. Podajemy funkcje. Wa�ny jest format!
   Przyk�ad: '- 5x_1 - 4x_2 - 6x_3'
  >  - funkcja umieszczona  w  ''
  >  - znaki +/- oddzielone spacja od wartosci
  >  - wspolczynniki zaraz przy x bez znaku *
  >  - kolejne zmienne x pisane w konwencji x_1,x_2,x_3 etc.

5. Podajemy ograniczenia. Wa�ny jest format!
   Przyk�ad: '1x_1 - 1x_2 + 1x_3 <= 20, 3x_1 + 2x_2 + 4x_3 <= 42, 3x_1 + 2x_2 <= 30'
  >  - ograniczenia umieszczone w ''
  >  - po przeciknu kolejne nierownosci
  >  - znaki - / + / <= / >= / = oddzielone spacj� od wartosci
  >  - wspoczlczynnki zaraz przy x
  >  - kolejne zmienne x pisane w konwencji x_1,x_2,x_3 etc.

W testach u�ywalismy np. przykladu z cwicze�:

'''
Przyklad z cwiczen:
f(x) = -5x1 - 4x2 - 6x3
z ograniczeniami:
x1 - x2 + x3 <= 20
3x1 + 2x2 + 4x3 <= 42
3x1 + 2x2 <= 30
x1>=0, x2>=0, x3>=0
'''

Aby go zasymulowa� nalezy wprowadzi�:


1. Wybierz typ wprowadzania: plik lub user:
> user
2. Max lub min: 
> min
3. Ilosc zmiennych w funkcji: 
> 3
4. Podaj funkcje (uwaga format!): 
> '- 5x_1 - 4x_2 - 6x_3'
5. Podaj ograniczenia (uwaga format!):
> '1x_1 - 1x_2 + 1x_3 <= 20, 3x_1 + 2x_2 + 4x_3 <= 42, 3x_1 + 2x_2 <= 30'


