from fractions import Fraction
import scipy.io
import pylab
import numpy as np
import sys
from numpy import *

class Simplex(object):
    def __init__(self, number_of_variables, constraints, function):
        self.output_file = open('output_simplex.txt', 'w')
        self.output_file.write("Rozwiazanie simplex:\n\n")

        self.num_vars = number_of_variables
        self.constraints = constraints
        self.objective = function[0]
        self.objective_function = function[1]
        self.coeff_matrix, self.r_rows, self.num_s_vars, self.num_r_vars = self.matrix_construct()
        self.martix_graph = []
        for a in self.coeff_matrix:
            self.martix_graph.append(a[:2]+ a[-1:])

        self.basic_vars = [0 for i in range(len(self.coeff_matrix))]
        self.start_cal()
        self.display()
        r_index = self.num_r_vars + self.num_s_vars

        for i in self.basic_vars:
            if i > r_index:
                raise ValueError("Rozwiazanie niewykonalne")

        if 'min' in self.objective.lower():
            self.solution = self.minimize()

        else:
            self.solution = self.maximize()
        self.optimize_val = self.coeff_matrix[0][-1]

        self.display()
        self.output_file.write('\nWyniki:\n')
        print 'Wyniki:'
        for zmn,val in self.solution.items():
            print zmn,val
            self.output_file.write(zmn + ': ')
            self.output_file.write(str(val))
            self.output_file.write('\n')

        print 'Wartosc funkcji celu:'
        print self.optimize_val
        self.output_file.write('\nWartosc funkcji celu: \n')
        self.output_file.write(str(self.optimize_val))

        if number_of_variables <= 2:
            self.martix_graph.pop(0)
            self.draw_graph()

    def equation(self, a, b, c, x):
        return (c - a*x)/b

    def draw_graph(self):
        line_list = []
        minv = float(min(self.solution.values()))
        maxv = float(max(self.solution.values()))
        x = arange(minv-3, maxv+3, 0.1)
        for item in self.martix_graph:
            y = self.equation(float(item[0]),float(item[1]),float(item[2]),x)
            line_list.append(y)
        for line in line_list:
            pylab.plot(x, line)

        pylab.title('Simplex Graph')
        pylab.plot(minv,maxv,'ro')
        pylab.hlines(0, minv-3, maxv+3, color='k')
        pylab.vlines(0, -15, 15, color='k')
        pylab.autoscale(enable=True, axis='y')
        y4 = np.minimum(line_list[0], line_list[2])
        pylab.grid(True)
        pylab.fill_between(x,line_list[1] , y4,where=(x>0) & (line_list[1]>0) & (line_list[0]>0) & (line_list[2] > 0), color='grey', alpha='0.5')
        pylab.show()

    def matrix_construct(self):
        num_s_vars = 0
        num_r_vars = 0
        for expression in self.constraints:
            if '>=' in expression:
                num_s_vars += 1

            elif '<=' in expression:
                num_s_vars += 1
                num_r_vars += 1

            elif '=' in expression:
                num_r_vars += 1

            else:
                raise ValueError("Bledne rownanie!")


        total_vars = self.num_vars + num_s_vars + num_r_vars

        coeff_matrix = [[Fraction("0/1") for i in range(total_vars+1)] for j in range(len(self.constraints)+1)]
        s_index = self.num_vars
        r_index = self.num_vars + num_s_vars
        r_rows = []
        for i in range(1, len(self.constraints)+1):
            constraint = self.constraints[i-1].split(' ')

            for j in range(len(constraint)):

                if '_' in constraint[j]:
                    coeff, index = constraint[j].split('_')
                    if constraint[j-1] is '-':
                        coeff_matrix[i][int(index)-1] = Fraction("-" + coeff[:-1] + "/1")
                    else:
                        coeff_matrix[i][int(index)-1] = Fraction(coeff[:-1] + "/1")

                elif constraint[j] == '<=':
                    coeff_matrix[i][s_index] = Fraction("1/1")
                    s_index += 1

                elif constraint[j] == '>=':
                    coeff_matrix[i][s_index] = Fraction("-1/1")
                    coeff_matrix[i][r_index] = Fraction("1/1")
                    s_index += 1
                    r_index += 1
                    r_rows.append(i)

                elif constraint[j] == '=':
                    coeff_matrix[i][r_index] = Fraction("1/1")
                    r_index += 1
                    r_rows.append(i)

            coeff_matrix[i][-1] = Fraction(constraint[-1] + "/1")

        return coeff_matrix, r_rows, num_s_vars, num_r_vars

    def start_cal(self):
        r_index = self.num_vars + self.num_s_vars
        for i in range(r_index, len(self.coeff_matrix[0])-1):
            self.coeff_matrix[0][i] = Fraction("-1/1")
        for i in self.r_rows:
            self.coeff_matrix[0] = add_row(self.coeff_matrix[0], self.coeff_matrix[i])
            self.basic_vars[i] = r_index
            r_index += 1
        s_index = self.num_vars
        for i in range(1, len(self.basic_vars)):
            if self.basic_vars[i] == 0:
                self.basic_vars[i] = s_index
                s_index += 1

        key_column = max_index(self.coeff_matrix[0])
        condition = self.coeff_matrix[0][key_column] > 0

        while condition is True:
            self.display()
            key_row = self.find_key_in_row(key_column = key_column)
            self.basic_vars[key_row] = key_column
            pivot = self.coeff_matrix[key_row][key_column]
            self.normalize(key_row, pivot)
            self.key_column_zero(key_column, key_row)

            key_column = max_index(self.coeff_matrix[0])
            condition = self.coeff_matrix[0][key_column] > 0

    def find_key_in_row(self, key_column):
        min_val = float("inf")
        min_i = 0
        for i in range(1, len(self.coeff_matrix)):
            if self.coeff_matrix[i][key_column] > 0:
                val = self.coeff_matrix[i][-1] / self.coeff_matrix[i][key_column]
                if val <  min_val:
                    min_val = val
                    min_i = i
        if min_val == float("inf"):
            raise ValueError("Rozwiazanie nieograniczone")

        return min_i

    def normalize(self, key_row, pivot):
        for i in range(len(self.coeff_matrix[0])):
            self.coeff_matrix[key_row][i] /= pivot

    def key_column_zero(self, key_column, key_row):
        num_columns = len(self.coeff_matrix[0])
        for i in range(len(self.coeff_matrix)):
            if i != key_row:
                factor = self.coeff_matrix[i][key_column]
                for j in range(num_columns):
                    self.coeff_matrix[i][j] -= self.coeff_matrix[key_row][j] * factor

    def function_update(self):
        objective_function_coeffs = self.objective_function.split()
        for i in range(len(objective_function_coeffs)):
            if '_' in objective_function_coeffs[i]:
                coeff, index = objective_function_coeffs[i].split('_')
                if objective_function_coeffs[i-1] is '-':
                    self.coeff_matrix[0][int(index)-1] = Fraction(coeff[:-1] + "/1")
                else:
                    self.coeff_matrix[0][int(index)-1] = Fraction("-" + coeff[:-1] + "/1")

    def minimize(self):
        self.function_update()

        for row, column in enumerate(self.basic_vars[1:]):
            if self.coeff_matrix[0][column] != 0:
                self.coeff_matrix[0] = add_row(self.coeff_matrix[0], multiply_const_row(-self.coeff_matrix[0][column], self.coeff_matrix[row+1]))

        key_column = max_index(self.coeff_matrix[0])
        condition = self.coeff_matrix[0][key_column] > 0

        while condition is True:
            self.display()
            key_row = self.find_key_in_row(key_column = key_column)
            self.basic_vars[key_row] = key_column
            pivot = self.coeff_matrix[key_row][key_column]
            self.normalize(key_row, pivot)
            self.key_column_zero(key_column, key_row)

            key_column = max_index(self.coeff_matrix[0])
            condition = self.coeff_matrix[0][key_column] > 0

        solution = {}
        for i, var in enumerate(self.basic_vars[1:]):
            if var < self.num_vars:
                solution['x_'+str(var+1)] = self.coeff_matrix[i+1][-1]

        for i in range(0, self.num_vars):
            if i not in self.basic_vars[1:]:
                solution['x_'+str(i+1)] = Fraction("0/1")
        return solution

    def maximize(self):
        self.function_update()

        for row, column in enumerate(self.basic_vars[1:]):
            if self.coeff_matrix[0][column] != 0:
                self.coeff_matrix[0] = add_row(self.coeff_matrix[0], multiply_const_row(-self.coeff_matrix[0][column], self.coeff_matrix[row+1]))

        key_column = min_index(self.coeff_matrix[0])
        condition = self.coeff_matrix[0][key_column] < 0

        while condition is True:
            self.display()
            key_row = self.find_key_in_row(key_column = key_column)
            self.basic_vars[key_row] = key_column
            pivot = self.coeff_matrix[key_row][key_column]
            self.normalize(key_row, pivot)
            self.key_column_zero(key_column, key_row)

            key_column = min_index(self.coeff_matrix[0])
            condition = self.coeff_matrix[0][key_column] < 0

        solution = {}
        for i, var in enumerate(self.basic_vars[1:]):
            if var < self.num_vars:
                solution['x_'+str(var+1)] = self.coeff_matrix[i+1][-1]

        for i in range(0, self.num_vars):
            if i not in self.basic_vars[1:]:
                solution['x_'+str(i+1)] = Fraction("0/1")

        return solution

    def display(self):
        print '\n Simplex table:'
        for row in self.coeff_matrix:
            for element in row:
                print '{:6}'.format(round(float(element),2)),
            print

        self.output_file.write('\n Simplex table:\n')
        for row in self.coeff_matrix:
            for element in row:
                self.output_file.write('{:6}'.format(round(float(element),2)))
            self.output_file.write('\n')

def add_row(row1, row2):
    row_sum = [0 for i in range(len(row1))]
    for i in range(len(row1)):
        row_sum[i] = row1[i] + row2[i]
    return row_sum

def max_index(row):
    max_i = 0
    for i in range(0, len(row)-1):
        if row[i] > row[max_i]:
            max_i = i

    return max_i

def multiply_const_row(const, row):
    mul_row = []
    for i in row:
        mul_row.append(const*i)
    return mul_row

def min_index(row):
    min_i = 0
    for i in range(0, len(row)):
        if row[min_i] > row[i]:
            min_i = i

    return min_i

class simplex_file:
    def __init__(self, obj, itype):
        self.obj = [1] + obj
        self.rows = []
        self.cons = []
        self.itype = itype
        self.equation_one = []
        self.equation_two = []

        self.sfile = open('simplex_output_file.txt', 'w')
        self.sfile.seek(0)
        self.sfile.truncate()

    def add_constraint(self, expression, value):
        self.rows.append([0] + expression)
        self.cons.append(value)

    def _pivot_column(self):
        low = 0
        idx = 0
        for i in range(1, len(self.obj)-1):
            if self.obj[i] < low:
                low = self.obj[i]
                idx = i
        if idx == 0:
            return -1
        return idx

    def _pivot_row(self, col):
        rhs = [self.rows[i][-1] for i in range(len(self.rows))]
        lhs = [self.rows[i][col] for i in range(len(self.rows))]
        ratio = []
        for i in range(len(rhs)):
            if lhs[i] == 0:
                ratio.append(99999999 * abs(max(rhs)))
                continue
            ratio.append(rhs[i]/lhs[i])
        return argmin(ratio)

    def save_to_file(self):
        data = matrix([self.obj] + self.rows)
        formatted_data = '[\n'
        for row in data:
            data_list = row.tolist()
            for el in data_list:
                formatted_data += '['
                for i in el:
                    formatted_data += str(i)
                    formatted_data += ' '
                formatted_data += ']\n'

        formatted_data += ']\n\n'
        self.sfile.write(formatted_data)

    def display(self):
        self.save_to_file()
        print '\n', matrix([self.obj] + self.rows)

    def _pivot(self, row, col):
        e = self.rows[row][col]
        self.rows[row] /= e

        for r in range(len(self.rows)):
            if r == row:
                continue
            self.rows[r] = self.rows[r] - self.rows[r][col]*self.rows[row]
        self.obj = self.obj - self.obj[col]*self.rows[row]

    def _check(self):
        if min(self.obj[1:-1]) >= 0:
            return 1
        return 0

    def solve(self):
        for i in range(len(self.rows)):
            self.obj += [0]
            ident = [0 for r in range(len(self.rows))]
            ident[i] = 1
            self.rows[i] += ident + [self.cons[i]]
            self.rows[i] = array(self.rows[i], dtype=float)
        self.obj = array(self.obj + [0], dtype=float)

        self.display()
        while not self._check():
            c = self._pivot_column()
            r = self._pivot_row(c)
            self._pivot(r,c)
            self.display()

        self.sfile.close()

if __name__ == '__main__':
    type = raw_input('1. Wybierz typ wprowadzania: plik lub user: ')
    type = type.strip(' ').lower()

    if type not in ['plik','user','test1','test2']:
        print type + ' jest niepoprawny. Wylaczenie programu.'
        sys.exit()

    if type == 'test1':
        objective = ('min', '- 5x_1 - 4x_2 - 6x_3')
        constraints = ['1x_1 - 1x_2 + 1x_3 <= 20', '3x_1 + 2x_2 + 4x_3 <= 42', '3x_1 + 2x_2 <= 30']
        Simplex(number_of_variables=3, constraints=constraints, function=objective)
        sys.exit()

    if type == 'test2':
        objective = ('min', '4x_1 + 1x_2')
        constraints = ['3x_1 + 1x_2 = 3', '4x_1 + 3x_2 >= 6', '1x_1 + 2x_2 <= 4']
        Simplex(number_of_variables=2, constraints=constraints, function=objective)

    if type == 'user':
        objective = []
        temp_type = raw_input('2. max lub min: ')
        if temp_type not in ['max', 'min']:
            print type + ' jest niepoprawny. Wylaczenie programu.'
            sys.exit()
        nof = raw_input('3. Ilosc zmiennych w funkcji: ')
        func = raw_input('4. Podaj funkcje (uwaga format!): ')
        func = func.replace("'", "")
        objective.append(temp_type)
        objective.append(func)
        objective = tuple(objective)
        constraints = raw_input('5. Podaj ograniczenia (uwaga format!): \n')
        constraints = constraints.replace("'", "")
        constraints = list(constraints.split(','))
        Simplex(number_of_variables=int(nof), constraints=constraints, function=objective)
        print('Rozwiazanie zostalo zapisane w output_simplex.txt')
        sys.exit()


    if type == 'plik':
        char = -1
        name_file = raw_input('2.Nazwa pliku: ');
        temp_type = raw_input('3. max lub min: ')
        if temp_type not in ['max', 'min']:
            print type + ' jest niepoprawny. Wylaczenie programu.'
            sys.exit()

        try:
            matlab_file = scipy.io.loadmat(name_file)
        except IOError as e:
            print e
            sys.exit()

        A = matlab_file['A'].toarray()
        b = [val for sublist in matlab_file['b'] for val in sublist]
        lbounds = [val for sublist in matlab_file['lbounds'] for val in sublist]
        ubounds = [val for sublist in matlab_file['ubounds'] for val in sublist]
        function = [val for sublist in matlab_file['c'] for val in sublist]
        if temp_type == 'max':
            char = -1

        function = [int(f) * char for f in function]

        temp = simplex_file(function, temp_type)

        for i in range(len(b)):
            a = A[i].tolist()
            temp.add_constraint(a, b[i])

        temp.solve()
        sys.exit()
